import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable()
export class DataServiceService {

  public startDate = new Date();
  public endDate = new Date();
  public daysOfWeek : Number[] = [];

  public startDateChange: Subject<Date> = new Subject<Date>();
  public endDateChange: Subject<Date> = new Subject<Date>();
  public daysOfWeekChange: Subject<Number[]> = new Subject<Number[]>();


  constructor() {
    this.startDateChange.subscribe((value) => {
      this.startDate = value
    });

    this.endDateChange.subscribe((value) => {
      this.endDate = value
    });

    this.daysOfWeekChange.subscribe((value) => {
      this.daysOfWeek = value;
    });
  }

  toggleDate() {
    this.startDateChange.next(this.startDate);
    this.endDateChange.next(this.endDate);
    this.daysOfWeekChange.next(this.daysOfWeek);
  }


}
