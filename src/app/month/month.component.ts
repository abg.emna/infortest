import {
  Component,
  HostBinding,
  ViewChild
} from '@angular/core';
import { DatePipe } from '@angular/common'

// @ts-ignore
import { SohoBarComponent, SohoCalendarComponent, SohoToastService} from 'ids-enterprise-ng';
import {DataServiceService} from "../data-service.service";

@Component({
  selector: 'app-month',
  templateUrl: 'month.component.html',
})
export class MonthComponent {

  @HostBinding('style.overflow') overflow = 'auto';
  @HostBinding('style.height') height = 'auto';
  @HostBinding('style.display') block = 'block';

  @ViewChild(SohoCalendarComponent) sohoCalendarComponent?: SohoCalendarComponent;

  constructor( private toastService: SohoToastService, private data: DataServiceService, private datepipe: DatePipe) {

    this.data.daysOfWeekChange.subscribe(value => {
      this.dayLeg[1].dayOfWeek = value;

      if (this.sohoCalendarComponent) {
        this.sohoCalendarComponent.markForRefresh()
      }
    });

    this.data.startDateChange.subscribe(value => {
      this.startDate = value;
      this.datepipe.transform(this.startDate, 'yyyy-MM-dd');
      this.dayLeg[2].dates = this.getDatesInRange(new Date(this.startDate), new Date(this.endDate));

      if (this.sohoCalendarComponent) {
        this.sohoCalendarComponent.markForRefresh()
      }
    });

    this.data.endDateChange.subscribe(value => {
      this.endDate = value;
      this.datepipe.transform(this.endDate, 'yyyy-MM-dd');
      this.dayLeg[2].dates = this.getDatesInRange(new Date(this.startDate), new Date(this.endDate));

      if (this.sohoCalendarComponent) {
        this.sohoCalendarComponent.markForRefresh()
      }
    });
    }

  public initialMonth = new Date().getMonth();
  public initialYear = 2022;
  public showViewChanger = true;
  public events?: [];
  public iconTooltip = 'status';
  public eventTooltip = 'comments';
  public dateFilter = true;
  public dates = {dates: [this.data.startDate.toDateString(), this.data.endDate.toDateString()]};
  public startDate = new Date();
  public endDate = new Date();
  public daysOfWeek : Number[] = [];
  public dayLeg = [
                    {"name":"Selected Day", "color": "#32a852", "dates": [], "dayOfWeek": []},
                    {"name":"Not Selected Days", "color": "#fff", "dates": [], "dayOfWeek": [1]},
                    {"name":"Selected Days", "color": "#32a852", "dates": [this.startDate, this.endDate], "dayOfWeek": this.daysOfWeek}
                  ];

  public getDatesInRange(startDate: Date, endDate: Date) {
    const date = new Date(startDate.getTime());
    const dates = [];

    while (date <= endDate) {
      dates.push(new Date(date));
      date.setDate(date.getDate() + 1);
    }
    return dates;
  }

  public onCalendarDateSelectedCallback = (_node: Node, args: SohoCalendarDateSelectedEvent) => {
    console.log('onCalendarEventSelectedCallback', args);
  }

  onRenderMonth(event: SohoCalendarRenderMonthEvent) {
    console.log('onRenderMonth', event);
  }

  onSelected(event: SohoCalendarDateSelectedEvent) {
    let selectedDate = new Date();

    if (event.month && event.day && event.year) {
      selectedDate.setMonth(event.month);
      selectedDate.setDate(event.day);
      selectedDate.setFullYear(event.year);
    }
    this.dayLeg[0].dates.push(selectedDate);
    if (this.sohoCalendarComponent) {
      this.sohoCalendarComponent.markForRefresh()
    }
  }

  onEventClicked(event: SohoCalendarEventClickEvent) {
    this.toastService.show({ title: 'Calendar Test', message: 'Event "' + event?.event?.subject + '" Clicked' });
    console.log('onEventClick', event);
  }

  onEventDblClicked(event: SohoCalendarEventClickEvent) {
    this.toastService.show({ title: 'Calendar Test', message: 'Event "' + event?.event?.subject + '" Double Clicked' });
    console.log('onEventDblClick', event);
  }

  onCalendarEventContextMenu(event: SohoCalendarEventClickEvent) {
    if (event) {
      this.toastService.show({ title: 'Calendar Test', message: 'Event "' + event?.event?.subject + '" ContextMenu' });
      console.log('onEventContextMenu', event);
    }
  }
}
