import { Component, OnInit } from '@angular/core';
import {DataServiceService} from "../data-service.service";

@Component({
  selector: 'app-days-of-week',
  templateUrl: './days-of-week.component.html',
  styleUrls: ['./days-of-week.component.css']
})
export class DaysOfWeekComponent implements OnInit {
  showModel = false;

public model = {
    mon: true,
    tue: true,
    wed: true,
    thu: true,
    fri: true,
    sat: true,
    sun: true,
  };

public id1 = 'checkbox1';
public id2 = 'checkbox2';
public id3 = 'checkbox3';
public id4 = 'checkbox4';
public id5 = 'checkbox5';
public id6 = 'checkbox6';
public id7 = 'checkbox7';

public checkBoxDisabled = false;
  constructor(private data: DataServiceService) { }
  ngOnInit() { }

  setDisable() {
    this.checkBoxDisabled = true;
  }

  setEnable() {
    this.checkBoxDisabled = false;
  }


  onUpdated(_event: SohoCheckBoxEvent) {
    console.log('CheckboxDemoComponent.onUpdated');
  }

  toggleModel() {
    this.showModel = !this.showModel;
  }

  modifyDays(dayNum: Number) {
    if (this.data.daysOfWeek.indexOf(dayNum) == -1) {
      this.data.daysOfWeek.push(dayNum);
    } else {
      this.data.daysOfWeek.splice(this.data.daysOfWeek.indexOf(dayNum), 1);
    }
    this.data.toggleDate();
  }
}
