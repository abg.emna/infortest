import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DateLegendComponent } from './date-legend.component';

describe('DateLegendComponent', () => {
  let component: DateLegendComponent;
  let fixture: ComponentFixture<DateLegendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DateLegendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateLegendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
